namespace :data do
  desc 'Create apikey for authenticating'
  task :apikey => :environment do
    ['Android Apikey', 'iOS Apikey', 'Website Apikey'].each do |platform|
      ApiKey.create(platform: platform)
    end
  end

  task :sample => :environment do

  end

  task :categories => :environment do
    ['Scarce book', 'Novel', 'Love story', 'Business', 'Language', 'Skill', 'Comics'].each do |name|
      Category.create(name: name)
    end
  end
end
