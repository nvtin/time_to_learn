source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'
# Use sqlite3 as the database for Active Record
gem 'mysql2'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'bootstrap-sass', '~> 3.3.6'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'
gem 'figaro'
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
gem "font-awesome-rails"
group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'simplecov', require: false
  gem 'rspec-rails', '~> 3.4'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'faker'
  gem 'shoulda-matchers', require: false
  gem 'factory_girl_rails'
  gem 'annotate'
  gem 'faker'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'rubocop', require: false
  gem 'brakeman', :require => false
end

gem 'devise'
gem 'omniauth' # required for devise_token_auth
gem 'paperclip', git: 'git://github.com/thoughtbot/paperclip.git'

# Grape API
gem 'grape'
gem 'grape-entity'
gem 'grape-swagger', github: 'tim-vandecasteele/grape-swagger'
gem 'grape-middleware-logger'
gem 'kramdown'
gem 'grape-swagger-rails'
# For Grape::Entity ( https://github.com/ruby-grape/grape-entity )
gem 'grape-swagger-entity'
# For representable ( https://github.com/apotonick/representable )
gem 'grape-swagger-representable'

gem 'friendly_id', '~> 5.1.0'
gem 'rails_admin'
gem 'globalize'
gem 'rails_admin_globalize'
gem 'ckeditor'
gem 'rails_admin_globalize_field'
gem 'haml'
gem 'active_model_serializers'
gem 'aws-sdk'
gem 'sidekiq'
gem 'devise-async'
gem 'grape_session'
gem 'angular-rails-templates'

source 'https://rails-assets.org' do
  gem 'rails-assets-bootstrap'
  gem 'rails-assets-angular'
  gem 'rails-assets-angular-translate'
  gem 'rails-assets-angular-ui-router'
  gem 'rails-assets-angular-route'
  gem 'rails-assets-angular-resource'
end
