# encoding: utf-8
require "rails_helper"

describe V1::Tests, :type => :request do

  context "Test API" do
    before :each do
      @user = create(:user)
      request_params = {
        email: @user.email,
        password: 12345678
      }
      @request_headers = {
        'HTTP_ACCEPT' => 'application/json',
        'HTTP_CONTENT_TYPE' => 'application/json',
        'HTTP_APIKEY' => ENV['api_key'],
        'HTTP_USERTOKEN' => ""
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      @request_headers["HTTP_USERTOKEN"] = output["data"]['authentication_token']
    end

    # =========== New Test ========================
    it "create new test unsuccessfully with user's token is wrong" do
      new_test_params = {
        name: Faker::Name.name,
        description: Faker::Name.name
      }
      @request_headers["HTTP_USERTOKEN"] = Faker::Name.name
      post '/api/v1/tests.json', new_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.user_token_wrong')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "create new test unsuccessfully with user's name is nil" do
      new_test_params = {
        name: nil,
        description: Faker::Name.name
      }
      post '/api/v1/tests.json', new_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.test_name_is_required')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "created new test successfully" do
      test_name = Faker::Name.name
      test_description = Faker::Name.name
      new_test_params = {
        name: test_name,
        description: test_description
      }
      post '/api/v1/tests.json', new_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.created_test")
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq test_name
      expect(response_json["data"]['description']).to eq test_description
      expect(response_json["data"]['questions']).to eq []
    end

    it "created new test successfully with questions" do
      test_name = Faker::Name.name
      test_description = Faker::Name.name
      new_test_params = {
        name: test_name,
        description: test_description,
        questions: [
          {
            name: Faker::Name.name,
            description: Faker::Name.name,
            options: nil
          }
        ]
      }
      post '/api/v1/tests.json', new_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.created_test")
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq test_name
      expect(response_json["data"]['description']).to eq test_description
      expect(response_json["data"]['questions'].length).to eq 1
      expect(response_json["data"]['questions'][0]['options']).to eq []
    end

    it "created new test successfully with questions and options" do
      test_name = Faker::Name.name
      test_description = Faker::Name.name
      new_test_params = {
        name: test_name,
        description: test_description,
        questions: [
          {
            name: Faker::Name.name,
            description: Faker::Name.name,
            options: [
              {
                name: Faker::Name.name,
                is_name: false
              },
              {
                name: Faker::Name.name,
                is_name: true
              }
            ]
          }
        ]
      }
      post '/api/v1/tests.json', new_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.created_test")
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq test_name
      expect(response_json["data"]['description']).to eq test_description
      expect(response_json["data"]['questions'].length).to eq 1
      expect(response_json["data"]['questions'][0]['options'].length).to eq 2
    end

    # ====== Update Test ==============
    it "Update test unsuccessfully with user's token is wrong" do
      test = create(:test)
      update_test_params = {
        name: Faker::Name.name,
        description: Faker::Name.name
      }
      @request_headers["HTTP_USERTOKEN"] = Faker::Name.name
      post "/api/v1/tests/#{test.id}.json", update_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.user_token_wrong')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "Update test unsuccessfully with user's name is nil" do
      test = create(:test)
      update_test_params = {
        name: nil,
        description: Faker::Name.name
      }
      post "/api/v1/tests/#{test.id}.json", update_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.test_name_is_required')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "Updated test successfully" do
      test_name = Faker::Name.name
      test_description = Faker::Name.name
      test = create(:test)
      update_test_params = {
        name: test_name,
        description: test_description,
        questions: [
          {
            name: 'ABC',
            description: 'ABC',
            options: [
              {
                name: 'Options 1',
                is_answer: true
              }
            ]
          }
        ]
      }
      post "/api/v1/tests/#{test.id}.json", update_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.updated_test")
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq test_name
      expect(response_json["data"]['description']).to eq test_description
      expect(response_json["data"]['questions'].length).to eq 1
      expect(response_json["data"]['questions'][0]['name']).to eq 'ABC'
      expect(response_json["data"]['questions'][0]['options'][0]['is_answer']).to eq true
    end

    it "Updated test successfully with questions without options" do
      test_name = Faker::Name.name
      test_description = Faker::Name.name
      test = create(:test)
      update_test_params = {
        name: test_name,
        description: test_description,
        questions: [
          {
            name: 'ABC',
            description: 'ABC'
          }
        ]
      }
      post "/api/v1/tests/#{test.id}.json", update_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.updated_test")
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq test_name
      expect(response_json["data"]['description']).to eq test_description
      expect(response_json["data"]['questions'].length).to eq 1
      expect(response_json["data"]['questions'][0]['options'].length).to eq 0
    end

    it "Updated test successfully with questions and options" do
      test_name = Faker::Name.name
      test_description = Faker::Name.name
      test = create(:test)
      update_test_params = {
        name: test_name,
        description: test_description,
        questions: [
          {
            name: Faker::Name.name,
            description: Faker::Name.name,
            options: [
              {
                name: Faker::Name.name,
                is_name: false
              },
              {
                name: Faker::Name.name,
                is_name: true
              }
            ]
          },
          {
            name: Faker::Name.name,
            description: Faker::Name.name,
            options: [
              {
                name: Faker::Name.name,
                is_name: false
              },
              {
                name: Faker::Name.name,
                is_name: true
              }
            ]
          }
        ]
      }
      post "/api/v1/tests/#{test.id}.json", update_test_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.updated_test")
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq test_name
      expect(response_json["data"]['description']).to eq test_description
      expect(response_json["data"]['questions'].length).to eq 2
      expect(response_json["data"]['questions'][0]['options'].length).to eq 2
    end

    # ========== Delete Test =================
    it "Delete test successfully with right test's id" do
      test = create(:test)
      post "/api/v1/tests/#{test.id}/delete.json", nil, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.deleted_test")
    end

    it "Delete test unsuccessfully with wrong test's id" do
      test = create(:test)
      post "/api/v1/tests/999999/delete.json", nil, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.dont_existed_test')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    # =============Answer Test =============
    it "Answer test successfully" do
      new_test_params = {
        name: Faker::Name.name,
        description: Faker::Name.name,
        questions: [
          {
            name: Faker::Name.name,
            description: Faker::Name.name,
            options: [
              {
                name: Faker::Name.name,
                is_name: false
              },
              {
                name: Faker::Name.name,
                is_name: true
              }
            ]
          }
        ]
      }
      post '/api/v1/tests.json', new_test_params, @request_headers
      output = JSON.parse(response.body)
      test_id = output['data']['id']
      question_id = output['data']['questions'][0]['id']
      option_id = output['data']['questions'][0]['options'][0]['id']
      answer_params = {
        test_id: test_id,
        answer_options: [
          {
            question_id: question_id,
            value: option_id
          }
        ]
      }
      post "/api/v1/tests/#{test_id}/answer.json", answer_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.answer_successfully")
      expect(response_json["data"]['user_id']).to eq @user.id
      expect(response_json["data"]['test_id']).to eq test_id
    end
  end
end