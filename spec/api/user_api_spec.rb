# encoding: utf-8
require "rails_helper"

describe V1::Users, :type => :request do

  context "User API" do
    before :each do
      @user = create(:user)
      @request_headers = {
        'HTTP_ACCEPT' => 'application/json',
        'HTTP_CONTENT_TYPE' => 'application/json',
        'HTTP_APIKEY' => ENV['api_key'],
        'HTTP_USERTOKEN' => ""
      }
    end

    # ====== Authenticate with ApiKey
    it "fail with wrong api key" do
      @request_headers["HTTP_APIKEY"] = Faker::Name.name
      get "/api/v1/users/sign_in.json", nil, @request_headers
      output = JSON.parse(response.body)
      expect(output["success"]).to eq false
      expect(output["message"]).to eq I18n.t("error.invalid_api_key")
      expect(output["code"]).to eq I18n.t("error_code.invalid_api_key")
    end

    # ============= Login ====================
    it "login successful" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      expect(output["success"]).to eq true
      expect(output["message"]).to eq I18n.t('label.success')
      expect(output["data"]).not_to be_empty
      expect(output["data"]['email']).to eq @user.email
    end

    # ======== Create User ================
    it "create new user failed because user's token is invalid" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      new_user_params = {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        password: 12345678,
        confirm_password: 12345678,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = Faker::Name.name
      post '/api/v1/users.json', new_user_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.user_token_wrong')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "create new user failed because user's password or confirm_password is not match" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      new_user_params = {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        password: 12345678,
        confirm_password: 123456789,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = token
      post '/api/v1/users.json', new_user_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.confirm_password_not_match_or_nil')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "create new user failed because user's password is not set" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      new_user_params = {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        password: nil,
        confirm_password: nil,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = token
      post '/api/v1/users.json', new_user_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.confirm_password_not_match_or_nil')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "create new user failed because user's name is not set" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      new_user_params = {
        name: nil,
        email: Faker::Internet.email,
        password: 12345678,
        confirm_password: 12345678,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = token
      post '/api/v1/users.json', new_user_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.name_is_required')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "create new user failed because user's email is not set" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      new_user_params = {
        name: Faker::Name.name,
        email: nil,
        password: 12345678,
        confirm_password: 12345678,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = token
      post '/api/v1/users.json', new_user_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.email_is_required')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "created new user successfully with teacher role" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      user_name = Faker::Name.name
      user_email = Faker::Internet.email
      new_user_params = {
        name: user_name,
        email: user_email,
        password: 12345678,
        confirm_password: 12345678,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = token
      post '/api/v1/users.json', new_user_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t('label.success')
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq user_name
      expect(response_json["data"]['email']).to eq user_email
      expect(response_json["data"]['role']).to eq 'TEACHER'
    end

    it "created new user with default role is student successfully if we set role is nil" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      user_name = Faker::Name.name
      user_email = Faker::Internet.email
      new_user_params = {
        name: user_name,
        email: user_email,
        password: 12345678,
        confirm_password: 12345678,
        role: nil
      }
      @request_headers["HTTP_USERTOKEN"] = token
      post '/api/v1/users.json', new_user_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t('label.success')
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq user_name
      expect(response_json["data"]['email']).to eq user_email
      expect(response_json["data"]['role']).to eq 'STUDENT'
    end

    it "created new user with role is student successfully if we set role is 0" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      user_name = Faker::Name.name
      user_email = Faker::Internet.email
      new_user_params = {
        name: user_name,
        email: user_email,
        password: 12345678,
        confirm_password: 12345678,
        role: 0
      }
      @request_headers["HTTP_USERTOKEN"] = token
      post '/api/v1/users.json', new_user_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t('label.success')
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq user_name
      expect(response_json["data"]['email']).to eq user_email
      expect(response_json["data"]['role']).to eq 'STUDENT'
    end

    # ========= update user ==========

    it "update user failed because user's token is invalid" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      @user2 = create(:user)
      update_info_params = {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        password: 12345678,
        confirm_password: 12345678,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = Faker::Name.name
      post "/api/v1/users/#{@user2.id}.json", update_info_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.user_token_wrong')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "update user failed because user's password or confirm_password is not match" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      update_info_params = {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        password: 12345,
        confirm_password: 123456789,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = token
      @user2 = create(:user)
      post "/api/v1/users/#{@user2.id}.json", update_info_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.confirm_password_not_match_or_nil')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "update user failed because user's name is not set" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      update_info_params = {
        name: nil,
        email: Faker::Internet.email,
        password: 12345678,
        confirm_password: 12345678,
        role: 1
      }
      @user2 = create(:user)
      @request_headers["HTTP_USERTOKEN"] = token
      post "/api/v1/users/#{@user2.id}.json", update_info_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.name_is_required')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "update user failed because user's email is not set" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      update_info_params = {
        name: Faker::Name.name,
        email: nil,
        password: 12345678,
        confirm_password: 12345678,
        role: 1
      }
      @request_headers["HTTP_USERTOKEN"] = token
      @user2 = create(:user)
      post "/api/v1/users/#{@user2.id}.json", update_info_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.email_is_required')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "updated user successfully with teacher role" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      user_name = Faker::Name.name
      user_email = Faker::Internet.email
      update_info_params = {
        name: user_name,
        email: user_email,
        password: 12345678,
        confirm_password: 12345678,
        role: 1
      }
      @user2 = create(:user)
      @request_headers["HTTP_USERTOKEN"] = token
      post "/api/v1/users/#{@user2.id}.json", update_info_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t('label.success')
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq user_name
      expect(response_json["data"]['email']).to eq user_email
      expect(response_json["data"]['role']).to eq 'TEACHER'
    end

    it "updated user with default role is student successfully if we set role is nil" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      user_name = Faker::Name.name
      user_email = Faker::Internet.email
      update_info_params = {
        name: user_name,
        email: user_email,
        password: 12345678,
        confirm_password: 12345678,
        role: nil
      }
      @user2 = create(:user)
      @request_headers["HTTP_USERTOKEN"] = token
      post "/api/v1/users/#{@user2.id}.json", update_info_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t('label.success')
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq user_name
      expect(response_json["data"]['email']).to eq user_email
      expect(response_json["data"]['role']).to eq 'STUDENT'
    end

    it "updated user with role is student successfully if we set role is 0" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      user_name = Faker::Name.name
      user_email = Faker::Internet.email
      update_info_params = {
        name: user_name,
        email: user_email,
        password: 12345678,
        confirm_password: 12345678,
        role: 0
      }
      @user2 = create(:user)
      @request_headers["HTTP_USERTOKEN"] = token
      post "/api/v1/users/#{@user2.id}.json", update_info_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t('label.success')
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq user_name
      expect(response_json["data"]['email']).to eq user_email
      expect(response_json["data"]['role']).to eq 'STUDENT'
    end

    it "updated user with password and confirm_password are set nil" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      user_name = Faker::Name.name
      user_email = Faker::Internet.email
      update_info_params = {
        name: user_name,
        email: user_email,
        password: nil,
        confirm_password: nil,
        role: 0
      }
      @user2 = create(:user)
      @request_headers["HTTP_USERTOKEN"] = token
      post "/api/v1/users/#{@user2.id}.json", update_info_params, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t('label.success')
      expect(response_json["data"]).not_to be_empty
      expect(response_json["data"]['name']).to eq user_name
      expect(response_json["data"]['email']).to eq user_email
      expect(response_json["data"]['role']).to eq 'STUDENT'
    end

    # ============ Get list users ============================
    it "get list users failed with user's token is wrong" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      5.times do
        create(:user)
      end
      @request_headers["HTTP_USERTOKEN"] = Faker::Name.name
      get "/api/v1/users.json", nil, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.user_token_wrong')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "get list users successfully with user's token is right" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      5.times do
        create(:user)
      end
      @request_headers["HTTP_USERTOKEN"] = token
      get "/api/v1/users.json", nil, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.get_list_user")
      expect(response_json["code"]).to eq 200
      expect(response_json["data"].length).to eq 6 # 5 new users + current user
      expect(response_json["data"].map {|user| user['email']}).to include @user.email
    end

    # ============ Get user detail ============================
    it "get user detail failed with user's token is wrong" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      @user2 = create(:user)
      @request_headers["HTTP_USERTOKEN"] = Faker::Name.name
      get "/api/v1/users/#{@user2.id}.json", nil, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.user_token_wrong')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end

    it "get list user successfully with user's token is right" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      @user2 = create(:user)
      @request_headers["HTTP_USERTOKEN"] = token
      get "/api/v1/users/#{@user2.id}.json", nil, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t("success.get_user")
      expect(response_json["code"]).to eq 200
      expect(response_json["data"]['email']).to eq @user2.email
    end

    it "get user  detail failed with user's id is not exist" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      @request_headers["HTTP_USERTOKEN"] = token
      get "/api/v1/users/99999.json", nil, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq false
      expect(response_json["message"]).to eq I18n.t('error.dont_existed_user')
      expect(response_json["code"]).to eq I18n.t("error_code.unauthorized")
    end
    # ============= Logout ====================
    it "logout successful" do
      request_params = {
        email: @user.email,
        password: 12345678
      }
      post "/api/v1/users/sign_in.json", request_params, @request_headers
      output = JSON.parse(response.body)
      token = output["data"]['authentication_token']
      @request_headers["HTTP_USERTOKEN"] = token
      post "/api/v1/users/logout.json", nil, @request_headers
      response_json = JSON.parse(response.body)
      expect(response_json["success"]).to eq true
      expect(response_json["message"]).to eq I18n.t('success.logout')
      expect(response_json["code"]).to eq 200
    end
  end
end


