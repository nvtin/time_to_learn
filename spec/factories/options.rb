FactoryGirl.define do
  factory :option, :class => Option do  |f|
    f.name {Faker::Name.name}
    f.is_answer false
    f.question { create(:question) }
  end
end