FactoryGirl.define do
  factory :questions, :class => Question do  |f|
    f.name {Faker::Name.name}
    f.description {Faker::Name.name}
    f.test { create(:test) }
  end
end