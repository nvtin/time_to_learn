FactoryGirl.define do
  factory :test, :class => Test do  |f|
    f.name {Faker::Name.name}
    f.description {Faker::Name.name}
  end
end