FactoryGirl.define do
  factory :user, :class => User do  |f|
    f.name {Faker::Name.name}
    f.email {Faker::Internet.email}
    f.password 12345678
    f.role 1
  end
end