'use strict'

###*
 # @ngdoc overview
 # @name TimeToLearn
 # @description
 # #TimeToLearn
###

angular.module 'TimeToLearn', [
  'ngRoute'
  'ui.router'
  'ui.bootstrap'
  'templates'
  'ngResource'
  'ngStorage'
]

angular.module('TimeToLearn').config [
  '$compileProvider'
  ($compileProvider) ->
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(http?|javascript|mailto):/)
]

angular.module('TimeToLearn').run [
  '$rootScope'
  '$http'
  ($rootScope, $sessionStorage, $http, $translate) ->
    $rootScope.lang = 'en'
    $rootScope.baseUrl = 'http://localhost:3000'
]
