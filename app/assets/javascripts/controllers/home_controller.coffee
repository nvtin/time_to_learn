###*
 # @ngdoc function
 # @name TimeToLearn.controller:HomeController
 # @description
 # # HomeController
 # Controller of the TimeToLearn
###

(->
  HomeController = ($window, $scope, $timeout, $http, $location, AuthService) ->
    
  HomeController.$inject = ['$window', '$scope', '$timeout', '$http', '$location', 'AuthService']
  angular.module('TimeToLearn').controller('HomeController', HomeController)
)()
