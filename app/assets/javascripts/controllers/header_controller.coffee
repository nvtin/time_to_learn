###*
 # @ngdoc function
 # @name TimeToLearn.controller:HeaderController
 # @description
 # # HeaderController
 # Controller of the TimeToLearn
###

(->
  HeaderController = ($window, $rootScope, $scope, $location) ->

  HeaderController.$inject = ['$window', '$rootScope', '$scope', '$location']
  angular.module('TimeToLearn').controller('HeaderController', HeaderController)
)();
