###*
 # @ngdoc function
 # @name TimeToLearn.controller:ApplicationController
 # @description
 # # ApplicationController
 # Controller of the TimeToLearn
###

(->
  ApplicationController = ($window, $scope, $timeout, $http, AuthService) ->
    
  ApplicationController.$inject = ['$window', '$scope', '$timeout', '$http', 'AuthService']
  angular.module('TimeToLearn').controller('ApplicationController', ApplicationController)
)()
