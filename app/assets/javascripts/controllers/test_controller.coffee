###*
 # @ngdoc function
 # @name TimeToLearn.controller:TestController
 # @description
 # # TestController
 # Controller of the TimeToLearn
###

(->
  TestController = ($scope, $http, $rootScope, $location, Test) ->

    $scope.loadTests = ->
      $scope.tests = Test.all {}
    $scope.loadTests()

    $scope.initTest = ->
      $scope.test = new Test
      $scope.test.name = ''
      $scope.test.description = ''
      $scope.test.questions = []
    $scope.initTest()

    $scope.addTest = () ->
      $http.post("#{$rootScope.baseUrl}/api/v1/tests", $scope.test).then (response) ->
        if response.data.code == 200
          $scope.test_id = response.data.data.id
          $location.path("/tests/#{$scope.test_id}")
        else
          $scope.errorMessage = response.data.message

    $scope.addQuestion = ->
      $scope.test.questions.push(name: '', description: '', options: [])

    $scope.addOption = ($event, $index) ->
      $event.preventDefault()
      $scope.test.questions[$index].options.push(name: '', is_answer: false)

    $scope.deleteTest = (test) ->
      testId = test.id
      $http.post("#{$rootScope.baseUrl}/api/v1/tests/#{testId}/delete").then (response) ->
        if response.data.code == 200
          $scope.loadTests()

    $scope.removeQuestion = (index)->
      $scope.test.questions.splice(index, 1)

    $scope.removeOption = (questionIndex, optionIndex)->
      $scope.test.questions[questionIndex].options.splice(optionIndex, 1)

  TestController.$inject = ['$scope', '$http', '$rootScope', '$location', 'Test']
  angular.module('TimeToLearn').controller('TestController', TestController)
)()

###*
 # @ngdoc function
 # @name TimeToLearn.controller:TestDetailController
 # @description
 # # TestDetailController
 # Controller of the TimeToLearn
###
(->
  TestDetailController = ($window, $scope, $timeout, $http, $rootScope, $location, $routeParams, Test) ->
    $scope.loadTest = ->
      $http.get("#{$rootScope.baseUrl}/api/v1/tests/#{$routeParams.testId}").then (response) ->
        $scope.test = response.data.data

    $scope.loadTest()

    $scope.updateTest = () ->
      $http.post("#{$rootScope.baseUrl}/api/v1/tests/#{$routeParams.testId}", $scope.test).then (response) ->
        if response.data.code == 200
          $scope.test_id = response.data.data.id
          $location.path("/tests/#{$scope.test_id}")
        else
          $scope.errorMessage = response.data.message

    $scope.deleteTest = (test) ->
      testId = test.id
      $http.post("#{$rootScope.baseUrl}/api/v1/tests/#{testId}/delete").then (response) ->
        if response.data.code == 200
          $location.path("/tests")
        else
          $scope.errorMessage = response.data.message
    $scope.addQuestion = ->
      $scope.test.questions.push(name: '', description: '', options: [])

    $scope.addOption = ($event, $index) ->
      $event.preventDefault()
      $scope.test.questions[$index].options.push(name: '', is_answer: false)

    $scope.removeQuestion = (index)->
      $scope.test.questions.splice(index, 1)

    $scope.removeOption = (questionIndex, optionIndex)->
      $scope.test.questions[questionIndex].options.splice(optionIndex, 1)

  TestDetailController.$inject = ['$window', '$scope', '$timeout', '$http', '$rootScope', '$location', '$routeParams', 'Test']
  angular.module('TimeToLearn').controller('TestDetailController', TestDetailController)
)()