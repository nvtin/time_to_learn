###*
 # @ngdoc function
 # @name TimeToLearn.controller:AuthController
 # @description
 # # AuthController
 # Controller of the TimeToLearn
###
(->
  AuthController = ( $scope, $http, $rootScope, $location, AuthService) ->
    $scope.showSignIn = AuthService.showSignIn
    $scope.$on 'logined', (event, args) ->
      $scope.showSignIn = !$scope.showSignIn

    $scope.user =
      email: ''
      password: ''

    $scope.errorMessage = ''

    $scope.setCurrentUser = (user) ->
      $scope.currentUser = user

    $scope.login = ->
      if $scope.user.email == ''
        $scope.errorMessage = 'Please enter email'
        return false

      if $scope.user.password == ''
        $scope.errorMessage = 'Please enter password'
        return false

      AuthService.login($scope.user).then (responde) ->
        if responde.code == 200
          $scope.setCurrentUser responde.data
          $scope.$emit('logined', { isLogined: true })
          $location.path('/users')
        else
          $scope.errorMessage = responde.message
      , ->
        $location.path('/users/login')
        $scope.errorMessage = 'email or password is incorrect.'

    $scope.logout = ->
      AuthService.logout()
      $location.path('/users/login')
      false

  AuthController.$inject = [ '$scope', '$http', '$rootScope', '$location', 'AuthService']
  angular.module('TimeToLearn').controller('AuthController', AuthController)
)()