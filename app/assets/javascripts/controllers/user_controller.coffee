###
 # @ngdoc function
 # @name TimeToLearn.controller:UserController
 # @description
 # # UserController
 # Controller of the TimeToLearn
###

(->
  UserController = ($scope, $http, $rootScope, $location, User) ->
    $scope.roles =
      [
        {label: 'Student', value: 0},
        {label: 'Teacher', value: 1}
      ]
    $scope.role = $scope.roles[0].value

    $scope.loadUsers = ->
      $scope.users = User.all {}
    $scope.loadUsers()

    $scope.initUser = ->
      $scope.user = new User
    $scope.initUser()

    $scope.addUser = () ->
      $http.post("#{$rootScope.baseUrl}/api/v1/users", $scope.user).then (response) ->
        if response.data.code == 200
          $scope.user_id = response.data.data.id
          $location.path("/users/#{$scope.user_id}")
        else
          $scope.errorMessage = response.data.message
    $scope.deleteUser = (user) ->
      userId = user.id
      $http.post("#{$rootScope.baseUrl}/api/v1/users/#{userId}/delete").then (response) ->
        if response.data.code == 200
          $scope.loadUsers()

  UserController.$inject = ['$scope', '$http', '$rootScope', '$location', 'User']
  angular.module('TimeToLearn').controller('UserController', UserController)
)()

###
 # @ngdoc function
 # @name TimeToLearn.controller:UserDetailController
 # @description
 # # UserDetailController
 # Controller of the TimeToLearn
###
(->
  UserDetailController = ($scope, $http, $rootScope, $location, $routeParams, User) ->
    $scope.roles = [
      {label: 'Student', value: 0},
      {label: 'Teacher', value: 1}
    ]
    $scope.loadUser = ->
      $http.get("#{$rootScope.baseUrl}/api/v1/users/#{$routeParams.userId}").then (response) ->
        $scope.user = response.data.data
    $scope.loadUser()

    $scope.updateUser = () ->
      $http.put("#{$rootScope.baseUrl}/api/v1/users/#{$routeParams.userId}", $scope.user).then (response) ->
        if response.data.code == 200
          $scope.user_id = response.data.data.id
          $location.path("/users/#{$scope.user_id}")
        else
          $scope.errorMessage = response.data.message

    $scope.deleteUser = (user) ->
      userId = user.id
      $http.post("#{$rootScope.baseUrl}/api/v1/users/#{userId}/delete").then (response) ->
        if response.data.code == 200
          $location.path("/users")
        else
          $scope.errorMessage = response.data.message

  UserDetailController.$inject = ['$scope', '$http', '$rootScope', '$location', '$routeParams', 'User']
  angular.module('TimeToLearn').controller('UserDetailController', UserDetailController)
)()