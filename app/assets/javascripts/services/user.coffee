app = angular.module 'TimeToLearn'

app.factory 'User', ['$resource',
  ($resource) ->

    $resource '/api/v1/users', {},
      all:
        isJson: true
]