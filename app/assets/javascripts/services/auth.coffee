###*
 # @ngdoc function
 # @name TimeToLearn.Service:AuthService
 # @description
 # # AuthService
 # Service of the TimeToLearn
###
(->
  AuthService = ($http, $rootScope, Session) ->
    authService = {}
    authService.showSignIn = false

    authService.login = (params) ->
      $http.post("#{$rootScope.baseUrl}/api/v1/users/sign_in", params).then (response) ->
        response = response.data
        if response.code == 200
          Session.create response.data.authentication_token, response.data.id, response.data.role
          response
        else
          response

    authService.isAuthenticated = ->
      !!Session.userId

    authService.isAuthorized = (authorizedRoles) ->
      if !angular.isArray(authorizedRoles)
        authorizedRoles = [authorizedRoles]
      authService.isAuthenticated() && authorizedRoles.indexOf(Session.userRole) != -1

    authService.logout = ->
      Session.destroy()

    authService

  AuthService.$inject = ['$http', '$rootScope', 'Session']
  angular.module('TimeToLearn').factory('AuthService', AuthService)
)();