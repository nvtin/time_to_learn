
(->
  Session = ($http, $rootScope, $localStorage, $location)->
    sessionService = {}

    sessionService.init = ->
      
      if $localStorage.sessionId
        sessionService.create($localStorage.sessionId, $localStorage.userId,
                              $localStorage.userRole)

    sessionService.create = (sessionId, userId, userRole) ->
      @id = sessionId
      @userId = userId
      @userRole = userRole
      $localStorage.sessionId = sessionId
      $localStorage.userId = userId
      $localStorage.userRole = userRole
      $http.defaults.headers.common['Usertoken'] = sessionId

    sessionService.destroy = ->
      @id = null
      @userId = null
      @userRole = null
      @sessionId = null
      $localStorage.sessionId = null
      $localStorage.userId = null
      $localStorage.userRole = null
      $http.defaults.headers.common['Usertoken'] = null

    sessionService.getCurrentUser = ->
      $localStorage.currentUser

    sessionService.setCurrentUser = (user) ->
      $localStorage.currentUser = user

    sessionService.init()
    sessionService
  Session.$inject = ['$http', '$rootScope', '$localStorage', '$location']
  angular.module('TimeToLearn').factory('Session', Session)
)();
