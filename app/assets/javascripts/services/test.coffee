app = angular.module 'TimeToLearn'

app.factory 'Test', ['$resource',
  ($resource) ->
    $resource '/api/v1/tests', {id: '@id'},
      all:
        isJson: true
]
