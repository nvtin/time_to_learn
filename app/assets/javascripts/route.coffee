(->
  config = ($routeProvider, $stateProvider, AuthService) ->
    $routeProvider
      .when('/', {
        title: 'Time To Learn',
        templateUrl: 'views/home.html'
        controller: 'HomeController'
        authRequired: true
      })
      .when('/tests', {
        title: 'Time To Learn',
        templateUrl: 'views/tests/index.html'
        controller: 'TestController'
        authRequired: true
      })
      .when('/tests/new', {
        title: 'Time To Learn',
        templateUrl: 'views/tests/new.html'
        controller: 'TestController'
        authRequired: true
      })
      .when('/tests/:testId/edit', {
        title: 'Time To Learn',
        templateUrl: 'views/tests/edit.html'
        controller: 'TestDetailController'
        authRequired: true
      })
      .when('/tests/:testId', {
        title: 'Time To Learn',
        templateUrl: 'views/tests/show.html'
        controller: 'TestDetailController'
        authRequired: true
      })
      .when('/users', {
        title: 'TimeToLearn',
        templateUrl: 'views/users/index.html'
        controller: 'UserController'
        authRequired: true
      })
      .when('/users/login', {
        title: 'TimeToLearn',
        templateUrl: 'views/users/login.html'
        controller: 'AuthController'
        authRequired: true
      })
      .when('/users/:userId/edit', {
        title: 'TimeToLearn',
        templateUrl: 'views/users/edit.html'
        controller: 'UserDetailController'
        authRequired: true
      })
      .when('/users/new', {
        title: 'Time To Learn',
        templateUrl: 'views/users/new.html'
        controller: 'UserController'
        authRequired: true
      })
      .when('/users/:userId', {
        title: 'Time To Learn',
        templateUrl: 'views/users/show.html'
        controller: 'UserDetailController'
        authRequired: true
      })

  config.$inject = ['$routeProvider', '$stateProvider']
  angular.module('TimeToLearn').config(config)
  angular.module('TimeToLearn').run [
    '$http'
    '$rootScope'
    '$location'
    'AuthService'
    ($http, $rootScope, $location, AuthService) ->
      $http.defaults.headers.common['Apikey']= '93b534f25bba474f4938553460b6c03b'
      $rootScope.$on '$routeChangeStart', (event, next) ->
        if next.authRequired
          if !AuthService.isAuthenticated()
            $location.path('/users/login')
            AuthService.showSignIn = true
        else
          if AuthService.isAuthenticated()
            $location.path('/')
  ]

)()
