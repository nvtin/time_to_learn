module APIHelpers
  def api_authenticate
    error!(response_error(I18n.t("error.invalid_api_key"), I18n.t("error_code.invalid_api_key")), 403) unless headers['Apikey'] == ENV['api_key']
  end

  def authenticate_error!
    h = {'Access-Control-Allow-Origin' => '*',
         'Access-Control-Request-Method' => %w{GET POST OPTIONS}.join(',')}
    error!(response_error(I18n.t('error.unauthorized'), I18n.t('error_code.unauthorized')), 403)
  end

  def current_user
    @current_user ||= User.where(authentication_token: headers['Usertoken']).first
  end

  def authenticated
    if headers['Usertoken'] && User.where(authentication_token: headers['Usertoken']).first
      return true
    else
      error!(response_error(I18n.t("error.user_token_wrong"), I18n.t("error_code.unauthorized")), 403)
    end
  end
  def logger
    Grape::API.logger
  end

  def set_locale
    I18n.locale = :en
  end

  def force_utf8_params
    traverse = lambda do |object, block|
      if object.kind_of?(Hash)
        object.each_value { |o| traverse.call(o, block) }
      elsif object.kind_of?(Array)
        object.each { |o| traverse.call(o, block) }
      else
        block.call(object)
      end
      object
    end
    force_encoding = lambda do |o|
      o.force_encoding(Encoding::UTF_8) if o.respond_to?(:force_encoding)
    end
    traverse.call(params, force_encoding)
  end

  def authenticate_user
    authenticated
  end

  def access_denied!
    error! 'Access Denied', 401
  end

  def bad_request!
    error! 'Bad Request', 400
  end

  def forbidden_request!
    error! 'Forbidden', 403
  end

  def not_found!
    error! 'Not Found', 404
  end

  def invalid_request!(message)
    error! message, 422
  end

  def response_success(message = '', data = {})
    { success: true, message: message, data: data, code: 200 }
  end

  def response_success_pagination(message = '', data = {}, pagination)
    {
      success: true,
      message: message,
      data: data,
      total: pagination.total_count,
      total_pages: pagination.total_pages,
      next_page: pagination.next_page,
      code: 200
    }
  end

  def response_success_array(message = '', data = [])
    { success: true, message: message, data: data, code: 200 }
  end

  def response_error(message = '', error = 000)
    { success: false, message: message, code: error }
  end
end
