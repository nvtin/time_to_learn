module ParamsHelpers
  extend Grape::API::Helpers

  params :pagination do
    optional :page, type: Integer, desc: 'Current page'
    optional :per_page, type: Integer, desc: 'Number items on each page'
  end

  def page
    params[:page] || 1
  end

  def per_page
    params[:per_page] || 10
  end
end
