module V1
  module Entities
    class Option < Grape::Entity
      expose :id, documentation: { type: Integer, required: true, desc: 'ID of Option.' }
      expose :name, documentation: { type: String, required: true, desc: 'Name of option.' }
      expose :is_answer, documentation: { type: 'boolean', required: true, desc: 'This is answer' }
    end
  end
end
