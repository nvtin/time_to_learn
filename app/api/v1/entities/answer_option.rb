module V1
  module Entities
    class AnswerOption < Grape::Entity
      expose :id, documentation: { type: Integer, required: true, desc: 'ID of AnswerOption' }
      expose :question_id, documentation: { type: Integer, required: true, desc: 'Id of question.' }
      expose :value, documentation: { type: Array, required: true, desc: 'This is answer' }
    end
  end
end
