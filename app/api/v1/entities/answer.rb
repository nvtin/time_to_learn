module V1
  module Entities
    class Answer < Grape::Entity
      expose :user_id, documentation: { type: Integer, desc: 'ID of User.' }
      expose :test_id, documentation: { type: Integer, desc: 'Id of test' }
      expose :answer_options, using: Entities::AnswerOption, documentation: { type: Hash }
    end
  end
end
