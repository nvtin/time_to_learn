module V1
  module Entities
    class Test < Grape::Entity
      expose :id, documentation: { type: Integer, required: true, desc: 'ID of User.' }
      expose :name, documentation: { type: String, required: true, desc: 'Name of User.' }
      expose :description, documentation: { type: 'string', desc: 'Access token of user.' }
      expose :questions , using: Entities::Question, documentation: { type: Hash, required: true}, if: :detail
    end
  end
end
