module V1
  module Entities
    class Question < Grape::Entity
      expose :id, documentation: { type: Integer, required: true, desc: 'ID of Question.' }
      expose :name, documentation: { type: String, required: true, desc: 'Name of Question.' }
      expose :description, documentation: { type: String, desc: 'Description of Question.' }
      expose :options  do |question|
        Entities::Option.represent question.options
      end
    end
  end
end
