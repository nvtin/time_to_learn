module V1
  module Entities
    class User < Grape::Entity
      expose :id, documentation: { type: Integer, required: true, desc: 'ID of User.' }
      expose :email, documentation: { type: String, required: true, desc: 'Email of User.' }
      expose :name, documentation: { type: String, required: true, desc: 'Name of User.' }
      expose :user_role, as: :role, documentation: { type: String, required: true, desc: 'Role of User.' }
      expose :authentication_token, documentation: { type: 'string', desc: 'Access token of user.' }
    end
  end
end
