module V1
  class Users < Grape::API

    helpers ParamsHelpers
    helpers do
      def user_params
        ActionController::Parameters.new(params).permit(:name, :email, :password, :role)
      end
    end
    namespace :users do
      # Sign in
      desc 'User authentication',
        entity: Entities::User,
        http_codes: [
          [200, I18n.t("success.login"), Entities::User]
        ]
      params do
        requires :email, type: String, desc: 'email of user'
        requires :password, type: String, desc: 'Password of user'
      end
      post :sign_in do
        # Check email
        params[:email].downcase! if params[:email].present?
        user = User.where('email = ?', params[:email]).first
        error!(response_error(I18n.t('error.dont_existed_email_or_username'), 403), 403) if user.nil?

        # Check password
        unless user.valid_password?(params[:password])
          error!(response_error(I18n.t('error.wrong_login_detail'), 403), 200)
        end

        user.ensure_authentication_token!
        response_success(I18n.t('label.success'), Entities::User.represent(user, login: true))
      end

      # ==============================
      desc 'Sign out',
        notes: "
          ## Error code:

          * 403: #{I18n.t("errors.unauthenticate")}

        ",
        http_codes: [
          [200, I18n.t("success.logout")]
        ],
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
      end
      post :logout do
        authenticate_user

        current_user.clear_authentication_token
        response_success I18n.t('success.logout'), 200
      end

      # ======================
      desc 'New User',
        entity: Entities::User,
        http_codes: [
          [200, I18n.t("success.created"), Entities::User]
        ],
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        requires :name, type: String, desc: 'Name of user'
        requires :email, type: String, desc: 'Email of user'
        requires :password, type: String, desc: 'Password'
        requires :confirm_password, type: String, desc: 'Confirm password'
        requires :role, type: Integer, desc: 'Role of user'
      end
      post do
        authenticate_user

        unless (params[:password] && params[:password] == params[:confirm_password])
          error!(response_error(I18n.t('error.confirm_password_not_match_or_nil'), 403), 403)
        end

        error!(response_error(I18n.t('error.email_is_required'), 403), 403) if params[:email].nil?
        error!(response_error(I18n.t('error.name_is_required'), 403), 403) if params[:name].nil?

        user = User.new(user_params)
        if user.valid? && user.save
          response_success(I18n.t('label.success'), Entities::User.represent(user))
        else
          error!(response_error(user.errors.full_messages.first, 401), 200)
        end
      end

      # ======================
      desc 'Update User',
        entity: Entities::User,
        http_codes: [
          [200, I18n.t("success.created"), Entities::User]
        ],
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        optional :name, type: String, desc: 'Name of user'
        optional :email, type: String, desc: 'Email of user'
        optional :password, type: String, desc: 'Password'
        optional :confirm_password, type: String, desc: 'Confirm password'
        optional :role, type: Integer, desc: 'Role of user'
      end
      post ':id' do
        authenticate_user

        unless (params[:password] == params[:confirm_password])
          error!(response_error(I18n.t('error.confirm_password_not_match_or_nil'), 403), 403)
        end
        error!(response_error(I18n.t('error.email_is_required'), 403), 403) if params[:email].nil?
        error!(response_error(I18n.t('error.name_is_required'), 403), 403) if params[:name].nil?

        user = User.find_by(id: params[:id])
        error!(response_error(I18n.t('error.dont_existed_user'), 403), 403) if user.nil?
        if (user.present?)
          params.delete :password if params[:password].nil?
          params.delete :confirm_password
          if user.update_attributes(user_params)
            response_success(I18n.t('label.success'), Entities::User.represent(user, login: true))
          else
            error!(response_error(user.errors.full_messages.first, 401), 200)
          end
        else
          error!(response_error(I18n.t('error.user_not_found'), 403), 403)
        end
      end

      # =====================
      desc 'Get list users',
        entity: Entities::User,
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
      end
      get do
        authenticate_user
        users = User.all
        response_success I18n.t("success.get_list_user"), Entities::User.represent(users)
      end

      # =====================
      desc 'Get user detail',
        entity: Entities::User,
        http_codes: [
          [200, I18n.t("success.created"), Entities::User]
        ],
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        requires :user_id, type: String, desc: 'Id of user'
      end
      get ':user_id' do
        authenticate_user
        user = User.find_by(id: params[:user_id])
        error!(response_error(I18n.t('error.dont_existed_user'), 403), 403) if user.nil?
        response_success I18n.t("success.get_user"), Entities::User.represent(user)
      end
      # =====================
      desc 'Delete User',
        entity: Entities::User,
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        requires :id, type: String, desc: "ID of User"
      end
      post ':id/delete' do
        authenticate_user
        user = User.find_by_id(params[:id])
        return error!(response_error(I18n.t('error.dont_existed_user'), 403), 403) if user.nil?
        if user.destroy
          response_success I18n.t("success.deleted_user")
        else
          error!(response_error(user.errors.full_messages.first, 401), 200)
        end
      end
    end
  end
end
