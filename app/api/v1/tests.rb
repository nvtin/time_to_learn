require 'action_controller/metal/strong_parameters'
module V1
  class Tests < Grape::API

    helpers ParamsHelpers
    helpers do
      def test_params
        ActionController::Parameters.new(params).permit(:name, :description, {questions_attributes: [:name, :description, {options_attributes: [:name, :is_answer]}]})
      end

      def answer_params
        ActionController::Parameters.new(params).permit(:test_id,  {answer_options_attributes: [:question_id, :value]}).merge( user_id: current_user.id )
      end
    end
    namespace :tests do
      # =====================
      desc 'Get list tests',
        entity: Entities::Test,
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
      end
      get do
        authenticate_user
        tests = Test.all
        response_success I18n.t("success.get_list_tests"), Entities::Test.represent(tests)
      end
      # =====================
      desc 'Get test detail',
        entity: Entities::Test,
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        optional :id, type: Integer, desc: "ID of test"
      end
      get "/:id" do
        authenticate_user
        test = Test.find_by_id(params[:id])
        error!(response_error(I18n.t('error.dont_existed_test'), 403), 403) if test.nil?
        response_success I18n.t("success.get_test_detail"), Entities::Test.represent(test, detail: true)
      end

      # =====================
      desc 'New Test',
        entity: Entities::Test,
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        requires :name, type: String, desc: "ID of test"
        optional :description, type: String, desc: 'Description of test'
        optional :questions, type: Array do
          optional :name, type: String, desc: 'Name of question'
          optional :description, type: String, desc: 'Description of question'
          optional :options, type: Array do
            optional :name, type: String, desc: 'Name of option'
            optional :is_answer, type: Boolean, desc: 'is answer'
          end
        end
      end
      post do
        authenticate_user
        error!(response_error(I18n.t('error.test_name_is_required'), 403), 403) if params[:name].nil?
        params[:questions].map {|question| question[:options_attributes] = question.delete :options} if params[:questions]
        params[:questions_attributes] = params.delete :questions
        test = Test.new(test_params)
        if test.valid? && test.save
          response_success I18n.t("success.created_test"), Entities::Test.represent(test, detail: true)
          else
          error!(response_error(test.errors.full_messages.first, 401), 200)
        end
      end
      # =====================
      desc 'Update Test',
        entity: Entities::Test,
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        requires :name, type: String, desc: "ID of test"
        optional :description, type: String, desc: 'Description of test'
        optional :questions, type: Array do
          optional :name, type: String, desc: 'Name of question'
          optional :description, type: String, desc: 'Description of question'
          optional :options, type: Array do
            optional :name, type: String, desc: 'Name of option'
            optional :is_answer, type: Boolean, desc: 'is answer'
          end
        end
      end
      post ':id' do
        authenticate_user

        error!(response_error(I18n.t('error.test_name_is_required'), 403), 403) if params[:name].nil?

        test = Test.find_by_id(params[:id])
        error!(response_error(I18n.t('error.dont_existed_test'), 403), 403) if test.nil?

        params[:questions].map {|question| question[:options_attributes] = question.delete :options} if params[:questions]
        params[:questions_attributes] = params.delete :questions
        test.questions.delete_all

        if test.update_attributes(test_params)
          response_success I18n.t("success.updated_test"), Entities::Test.represent(test, detail: true)
          else
          error!(response_error(test.errors.full_messages.first, 401), 200)
        end
      end

      # =====================
      desc 'Delete Test',
        entity: Entities::Test,
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        requires :id, type: String, desc: "ID of test"
      end
      post ':id/delete' do
        authenticate_user
        test = Test.find_by_id(params[:id])
        return error!(response_error(I18n.t('error.dont_existed_test'), 403), 403) if test.nil?
        if test.destroy
          response_success I18n.t("success.deleted_test")
        else
          error!(response_error(test.errors.full_messages.first, 401), 200)
        end
      end

      # =====================
      desc 'Answer Test',
        # entity: Entities::Test,
        headers: {
          "UserToken" => {
            description: "User's authentication_token",
            type: String,
            required: true
          }
        }
      params do
        requires :test_id, type: Integer, desc: 'Id of Test'
        optional :answer_options, type: Array do
          optional :question_id, type: Integer, desc: 'Id of question'
          optional :value, type: String, desc: 'The answer (that is ids of options) user choosed'
        end
      end
      post ':test_id/answer' do
        authenticate_user
        test =  Test.find_by(id: params[:test_id])
        error!(response_error(I18n.t('error.dont_existed_test'), 403), 403) if test.nil?
        answer = Answer.where(test_id: params[:test_id], user_id: current_user.id).first
        params[:answer_options_attributes] = params.delete :answer_options
        if answer.present?
          answer.answer_options.delete_all
          answer.update_attributes(answer_params)
        else
          answer = Answer.create(answer_params)
        end
        response_success I18n.t("success.answer_successfully"), Entities::Answer.represent(answer)
      end
    end
  end
end