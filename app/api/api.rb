require 'grape'

module API
  class Base < Grape::API
    use Grape::Middleware::Logger

    rescue_from :all, backtrace: false do |e|
      message = (Rails.env == 'production' ? I18n.t('api.default_error') : e.message)
      Grape::API.logger.debug e
      if e.is_a? ActiveRecord::RecordNotFound
        Rack::Response.new({ success: false, message: message, code: 404 }.to_json, 200)
      else
        Rack::Response.new({ success: false, message: message, code: 403 }.to_json, 500)
      end
    end

    prefix 'api'
    format :json
    version 'v1', using: :path, vendor: 'TimeToLearn'

    helpers APIHelpers
    before do
      set_locale
      api_authenticate
      force_utf8_params
    end

    mount V1::Users
    mount V1::Tests


    add_swagger_documentation(
      base_path: "/",
      api_version: 'v1',
      hide_documentation_path: true,
      models: [V1::Entities::User],
      markdown: GrapeSwagger::Markdown::KramdownAdapter.new,
      info: {
        title: 'Time To Learn API Documentation',
        description: 'API for supporting Android, iOS and website of TimeToLearn system.',
      }
    )
  end
end
