class Answer < ActiveRecord::Base
  belongs_to :user
  belongs_to :test
  has_many :answer_options, dependent: :destroy
  accepts_nested_attributes_for :answer_options
end
