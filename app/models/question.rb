class Question < ActiveRecord::Base
  has_many :options, dependent: :destroy
  accepts_nested_attributes_for :options
  belongs_to :test
end
