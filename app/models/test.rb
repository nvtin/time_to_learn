class Test < ActiveRecord::Base
  has_many :questions, dependent: :destroy
  has_many :answers, dependent: :destroy
  accepts_nested_attributes_for :questions
end
