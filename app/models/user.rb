require 'bcrypt'
class User < ActiveRecord::Base
  include BCrypt
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable
  has_many :answers, dependent: :destroy

  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }

  before_save { self.email = email.downcase }

  class UserRole
    STUDENT = 0
    TEACHER = 1
  end

  RevertUserRole = {
    0 => 'STUDENT',
    1 => 'TEACHER'
  }

  def user_role
    role = self.role || 0
    RevertUserRole[role]
  end
  # Check teacher
  def is_teacher?
    self.role == UserRole::TEACHER
  end

  # Check student
  def is_student?
    self.role == UserRole::STUDENT
  end

  def ensure_authentication_token!
    # if authentication_token.blank?
    self.authentication_token = generate_authentication_token
    self.save validate: false
    # end
  end

  def clear_authentication_token
    update(authentication_token: nil)
  end

  private
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

end
