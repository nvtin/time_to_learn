class UserSerializer < ActiveModel::Serializer
  attributes :id, :provider, :uid, :name, :username,
    :avatar, :email, :phone, :address,
    :gender, :role, :birthday, :slug
end
