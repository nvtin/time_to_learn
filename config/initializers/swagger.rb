GrapeSwaggerRails.options.url      = "api/v1/swagger_doc"
GrapeSwaggerRails.options.app_name = 'TimeToLearn'
GrapeSwaggerRails.options.app_url  = '/'

GrapeSwaggerRails.options.api_key_name = 'Apikey'
GrapeSwaggerRails.options.api_key_type = 'header'

Grape::Entity.format_with :format_number do |item|
  item.to_i
end