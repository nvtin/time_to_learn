Rails.application.routes.draw do
  devise_for :users
  mount GrapeSwaggerRails::Engine => '/api_docs'

  mount API::Base => '/'

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
end
