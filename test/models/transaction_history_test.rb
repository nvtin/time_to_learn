# == Schema Information
#
# Table name: transaction_histories
#
#  id                  :integer          not null, primary key
#  payment_id          :integer
#  user_id             :integer
#  number              :string(255)
#  title               :string(255)
#  description         :text(65535)
#  total               :decimal(8, 2)    default("0.00")
#  shipping_address_id :integer
#  state               :string(255)      default("init")
#  transaction_type    :string(255)      default("Exchange")
#  transaction_at      :datetime
#  cancelled_at        :datetime
#  approved_at         :datetime
#  completed_at        :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  device_uid          :string(255)      default("")
#  slug                :string(255)
#

require 'test_helper'

class TransactionHistoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
