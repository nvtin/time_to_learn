# == Schema Information
#
# Table name: orders
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  number                 :string(255)
#  quantity               :integer          default("0")
#  total                  :decimal(8, 2)    default("0.00")
#  approved_at            :datetime
#  cancelled_at           :datetime
#  payment_at             :datetime
#  shipping_at            :datetime
#  completed_at           :datetime
#  transaction_history_id :integer
#  state                  :string(255)      default("init")
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  device_uid             :string(255)      default("")
#  slug                   :string(255)
#

require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
