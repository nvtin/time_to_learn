# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1.0.0

### How do I get set up? ###

* Bundle install: bundle install
* Configuration database in config/database.yml
* Run Migration: rake db:migrate
* Run Data Seed: rake db:seed
* Run Project: rails s
* Go to http://localhost:3000

* for API guild, please go to http://localhost:3000/api_docs to have API links
api_key: 93b534f25bba474f4938553460b6c03b

*
### Testing ###
* Rspec is applied for api
* Run Rspec to see results: rspec spec/
