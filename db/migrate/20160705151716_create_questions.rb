class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :test
      t.text :name
      t.text :description

      t.timestamps null: false
    end
    add_index :questions, :test_id
  end
end
