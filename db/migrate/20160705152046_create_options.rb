class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.references :question
      t.text :name
      t.boolean :is_answer, default: false

      t.timestamps null: false
    end
    add_index :options, :question_id
  end
end
