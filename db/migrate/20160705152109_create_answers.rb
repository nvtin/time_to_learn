class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.references :user
      t.references :test
      t.references :question
      t.string :value
      t.timestamps null: false
    end
    add_index :answers, :user_id
    add_index :answers, :test_id
    add_index :answers, :question_id
  end
end
