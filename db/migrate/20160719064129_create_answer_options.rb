class CreateAnswerOptions < ActiveRecord::Migration
  def change

    create_table :answer_options do |t|
      t.references :answer
      t.integer :question_id
      t.string :value
      t.timestamps null: false
    end
    add_index :answer_options, :answer_id
    remove_index :answers, :question_id

    change_table :answers do |t|
      t.remove :question_id
      t.remove :value
    end
  end
end
